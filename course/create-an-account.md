---
title: Getting Started
nav_order: 2
---

# Prerequisites
The following are important steps to take before doing this workshop. I have created or found videos to help you with them. 

## Sign up for a GitLab.com account
In order to complete the workshop, you'll need to have a GitLab.com account. If you already have an account on GitLab.com and wish to use your existing account, you do not need to complete this step.

#### Validation
Please note, GitLab requires validation using a credit or debit card in order to use certain features, including CI/CD pipelines. We do not charge the card and we do not keep it on file to charge later. It is being used as a way to prevent <a href="https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/" target="_blank">crypto mining abuse</a>. 

#### Steps

1. Visit <a href="https://gitlab.com" target="_blank">https://gitlab.com</a>
1. Click on Register now or Sign in with an existing account.
1. Follow steps to create or log into your account.

## Sign up for a Twitter Account

Your bot needs to be an actual Twitter account. If you already have one, you can practice automating with that account or you can create a new account on Twitter whose only function is to be a bot. 

#### Steps
1. Go to Twitter.com and select create new account. 
1. Follow instructions on screen to create an account. 

## Sign up for a Twitter Developer Account with Elevated Access

Twitter developer account gives your twitter account access to the Twitter API. There are a few permissions you'll need to update before you can get your keys. Keys are how the code your write is able to communicate with the account the keys are from. 

#### Steps 

1. Head to [t.co/developer](t.co/developer)
1. In the top right, you should see your Twitter profile picture. If you don't, click login and login in to your Twitter account. 
1. Once you've logged in, a popup will appear that asks some questions so you can register as a devloper. Follow the onscreen instructions. 
1. When the page shows you your keys, click "dashboard" and head to the main dashboard for your account. 
1. Inn the Dashboard, click your app under "porojects and Apps" on the file tree on the left. 
1. Scroll down to authentication and click "Set up"
1. Select Read and Write
1. Select automated app or bot in the next section
1. In the App Info section, you need to put URLs in the callback URL and Website URL section. 
1. Click Save
1. Back in the apps section, there is a keys and tokens tab. Click that. 
1. Regenerate your API Key and Secret and copy them to a secure location (A doc only you have acces to, notepad, .env file in your local repo)
1. Generate and save the Access Token and Access Token Secret


## Video for all the above

[Pj GitLab, Twitter, and Twitter Developer Access instructions](https://www.youtube.com/watch?v=VpkubSWAwOE)

## More instructions

In order to work locally on your code, you need to set up your computer to work with GitLab. The following videos from [Valentin Despa](https://vdespa.com/) will help you do so for Windows, Mac, or Linux. Thank you Valentin for these and [other helpful videos](https://www.youtube.com/channel/UCUUl_HXJjU--iYjUkIgEcTw) freely available on Youtube. 

- [Instructions for Mac](https://www.youtube.com/watch?v=_qDJ0W1wR5w)
- [Instructions for Windows](https://www.youtube.com/watch?v=Vmt0V6a3ppE)
- [Instructions for Linux](https://www.youtube.com/watch?v=iXuIp5uNnLk)


[Next Page](https://devops-education.gitlab.io/workshops/python-twitter-workshop/course/code-breakdown/)
