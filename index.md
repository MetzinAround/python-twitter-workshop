---
title: Home
nav_order: 1
description: Workshop on how to use Gitlab and work with Twitter API
permalink: /
---

# Overview
🚧 Pardon our dust while we Iterate 🚧

## Why make this workshop?

This workshop exists to be dropped into your class so students can gain experience with GitLab creating a Twitter bot with Python. Python is a popular language used in a variety of contexts, and this workshop is designed to give students an opportunity to work with a more advanced type of app, one that calls on API endpoints from one of the most popular social media sites, Twitter. Some skills and knowledge this training will help with

- git
- integrated IDEs in GitLab
- GitLab UI and conventions
- Understanding API calls
- using .env and environment variables to keep keys secure

## Python

This workshop expects students are familiar with the following concepts in Python:

- variables
- lists
- functions
- logic (If/else)
- try/except

The code can be found in the file [`bot.py`](/bot.py) in this project. This can be considered the answer key for students. The workshop will walk students through creating the bot through live coding modeling; the instructor will write the code live while talking about the thought process in creating it and referencing docs. It can also be run using the [PRIMM method from Sue Sentance](https://primmportal.com/). PRIMM stands for Predict, Run, Inverstigate, Modify, and Make. Students can be given the code and asked to make predictions on what the code will do when run. After discussion, the instructor will run the code and compare what was predicted with what happened during the run. After, students investigate the code with questions about the code, and finally they can modify and make their own version of the code by creating a unique twitter bot app on their own or in groups. 


[Next Page](https://devops-education.gitlab.io/workshops/python-twitter-workshop/course/create-an-account/)
