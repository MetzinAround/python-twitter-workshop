import tweepy
import config
import time
import schedule as sched

print("Bot Active")

#This is that V2 authentication required to give the bot access to the API
api_m = tweepy.Client(
    bearer_token = config.bearer_token,
    consumer_key = config.consumer_key,
    consumer_secret = config.consumer_secret,
    access_token = config.access_token,
    access_token_secret = config.access_token_secret
    )

#input the id of the twitter account that will be acting as a bot as an int
bot_id = ""
#write the name of your bot including the @ here. This variable will be used in a few places. 
bot_name = ""

filter_terms = [bot_name]

def tweeting_function():
    # function called sending an original tweet
    print("Sending Tweet!")
    message = "This is a Scheduled Tweet"
    api_m.create_tweet(text = message)
    print("Tweet Sent")

#uncommenting the next line means the bot will tweet automatically upon running the file.
#tweeting_function()

# The following line schedules a tweet for each day at 11am. Uncomment when using a schedule.
# sched.every().day.at("11:00").do(tweeting_function)

def commenting_function(twt):
    #the id of the tweet that activated the function
    the_id = twt.id
    #This will be the tweet that gets sent back to the author
    message = "Thanks for saying please!"
    #the text of the incoming tweet
    the_tweet = twt.text.lower()

    print("Tweet Found in stream!")
    #twt.author.screen_name, etc, are taken from Twitter docs LINK HERE
    #PRinting the name and text of the tweet found in stream. 
    print(f"TWEET: {twt.text}")
    #tweet has "please" and the bot's name in it
    if ("please" in the_tweet) and (bot_name in the_tweet):
        #another try/except to see if we're able to comment. If not, an error is printed to the console.
        try:
            print("Attempting Comment")
            api_m.create_tweet(text = message, in_reply_to_tweet_id = the_id)
            print("TWEET SUCCESSFUL")
        except Exception as err:
            print(err)
        #else for whether the tweet has `please` in it and if the bot was tagged in the tweet.
    else:
        print("Tweet wasn't nice enough")

#a subclass is required for a [Tweepy&Twitter stream](https://docs.tweepy.org/en/stable/streaming.html). We're creating a subclass of Stream called `MyStreamListener` with two parameters
class MyStreamListener(tweepy.StreamingClient):

    # on_tweet is what happens when a tweet comes into the stream
    def on_tweet(self, twts):
        commenting_function(twts)   
         
    # on connect says when the stream connects
    def on_connect(self):
        print("Stream Connected")

    # error handler
    def on_error(self, status):
        print(status)           


#create instance of stream class
stream_m = MyStreamListener(bearer_token= config.bearer_token, wait_on_rate_limit= True)

for term in filter_terms:
    stream_m.add_rules(tweepy.StreamRule(term))

stream_m.add_rules(tweepy.StreamRule(f"-is:retweet -is:quote lang:en"))

stream_m.filter(expansions=["author_id"])
#use "follow" to RT a specific account (for example: if you want to RT only tweets from a specific account, use `follow`), use "track" for tweets that contain the string included here. Note that track must be an array


sched.every().day.at("12:00").do(tweeting_function)

# Turns on the schedule
while True:
  sched.run_pending()
  time.sleep(1) 